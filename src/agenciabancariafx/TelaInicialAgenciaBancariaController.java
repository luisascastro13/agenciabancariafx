/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author luisa
 */
public class TelaInicialAgenciaBancariaController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private TextField cpfTitular, numeroConta;
        
    @FXML
    public void criarConta(ActionEvent event)
    {
        AgenciaBancariaFX.trocaTela("TelaCriarConta.fxml");
    }
    
    @FXML
    public void cadastrarPessoa(ActionEvent event)
    {
        AgenciaBancariaFX.trocaTela("TelaCadastrarPessoa.fxml");
    }
    
    @FXML
    public void entrarConta(ActionEvent event)
    {        
        for(int i=0; i<AgenciaBancariaFX.quantPessoas; i++)
        {
            if(cpfTitular.getText().equals(AgenciaBancariaFX.pessoas.get(i).getCpf()) && AgenciaBancariaFX.contas.get(i).getNumero() == (Integer.parseInt(numeroConta.getText())))
            {
                AgenciaBancariaFX.pessoaEntrarConta = AgenciaBancariaFX.pessoas.get(i);
                AgenciaBancariaFX.trocaTela("TelaConta.fxml");
            }            
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
