/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agenciabancariafx;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class TelaCriarContaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private TextField cpf, numero, digito;
    
    @FXML
    private Label mensagem;
            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    
    @FXML
    public void criarConta(ActionEvent event) 
    {
        Conta conta = null;
        for(int i=0; i<AgenciaBancariaFX.quantPessoas; i++)
        {
            if(cpf.getText().equals(AgenciaBancariaFX.pessoas.get(i).getCpf()))
            {
                conta = new Conta(AgenciaBancariaFX.pessoas.get(i));
                conta.setDigito(Integer.parseInt(digito.getText()));
                conta.setNumero(Integer.parseInt(numero.getText()));
                AgenciaBancariaFX.contas.add(conta);
                AgenciaBancariaFX.quantContas++;
               // mensagem.setText("Conta criada!"); 
                break;
            }            
        }
        if(conta == null)
        {
            //mensagem.setText("Titular não existe.");
            
        }
        AgenciaBancariaFX.trocaTela("TelaInicialAgenciaBancaria.fxml");
    }
}
