package agenciabancariafx;

import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AgenciaBancariaFX extends Application {
    private static Stage stage;
    public static ArrayList<Pessoa> pessoas;
    public static ArrayList<Conta> contas;
    public static int quantContas = 0;
    public static int quantPessoas = 0;
    public static Pessoa pessoaEntrarConta;
    
    public static Stage getStage() {
        return stage;
    }    
    public static void trocaTela(String tela){
        Parent root = null;
        try{
            root = FXMLLoader.load(AgenciaBancariaFX.class.getResource(tela));
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.show();
        }catch(IOException e){
            System.out.println("Verificar arquivo FXML. Erro " + e.getMessage());
        }
    }
    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        pessoas = new ArrayList<>();
        contas = new ArrayList<>();        
        
        Parent root = FXMLLoader.load(getClass().getResource("TelaInicialAgenciaBancaria.fxml"));
        
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args); 
    }    
}
