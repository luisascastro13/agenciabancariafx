package agenciabancariafx;

public class Conta {
    private Pessoa titular;
    private int numero, digito;
    private double saldo;
    
    Conta(Pessoa titular)
    {
        this.titular = titular;       
    }

    public Pessoa getTitular() {
        return titular;
    }

    public void setTitular(Pessoa titular) {
        this.titular = titular;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getDigito() {
        return digito;
    }

    public void setDigito(int digito) {
        this.digito = digito;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
    
    public boolean debita(double valor)    {
        boolean verifica;
        
        if(valor>saldo)
        {          
            verifica = false;
            System.out.println("Você não tem dinheiro suficiente para debitar esse valor.");
        }
        else
        {   
            saldo = saldo - valor;
            verifica = true;
        }
        return verifica; 
    }
    public void credita(double valor)    {
        saldo += valor;  
    }
    
//    public double faltouRetirar (double valor){
//       double x;
//       x = valor - saldo;       
//       return x;
//    }
//    ;
    
}
