package agenciabancariafx;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author luisa
 */
public class TelaCadastrarPessoaController implements Initializable {

    /**
     * Initializes the controller class.
     */
   
    @FXML
    private TextField cpf, bairro, nome, cidade, estado, fone, email, rg, endereco;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    public void cadastrarPessoa(ActionEvent event){
        Pessoa p = new Pessoa(cpf.getText());
        p.setBairro(bairro.getText());
        p.setCidade(cidade.getText());
        p.setCpf(cpf.getText());
        p.setEmail(email.getText());
        p.setEndereco(endereco.getText());
        p.setNome(nome.getText());
        p.setFone(Integer.parseInt(fone.getText()));
        p.setRg(rg.getText());
        p.setEstado(estado.getText());
        
        AgenciaBancariaFX.pessoas.add(p);
        AgenciaBancariaFX.quantPessoas++;
        
        //System.out.println(p); -->testando se está salvando na matriz
        AgenciaBancariaFX.trocaTela("TelaInicialAgenciaBancaria.fxml");
        
    }
    
}
