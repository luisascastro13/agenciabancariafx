package agenciabancariafx;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author luisa
 */

public class TelaContaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Label dados, nome, numero, cpf, saldo;
    @FXML
    private TextField valor;    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       try {
           dados.setText("" + AgenciaBancariaFX.pessoaEntrarConta);
       } catch ( Exception e ) {
           System.out.println("EXCEPTIOOOOOOON e: " + e);
       }
       for(int i=0;i<AgenciaBancariaFX.quantContas;i++)
       {
           if(AgenciaBancariaFX.pessoaEntrarConta.getNome().equals(AgenciaBancariaFX.contas.get(i).getTitular().getNome()))
           {
               numero.setText("" + AgenciaBancariaFX.contas.get(i).getNumero());
               saldo.setText("" + AgenciaBancariaFX.contas.get(i).getSaldo());
               nome.setText("" + AgenciaBancariaFX.contas.get(i).getTitular().getNome());
               cpf.setText("" + AgenciaBancariaFX.contas.get(i).getTitular().getCpf());
           }
       }  
    }
    public void creditar(){
        for(int i=0;i<AgenciaBancariaFX.quantContas;i++)
        {
            if(AgenciaBancariaFX.pessoaEntrarConta.getNome().equals(AgenciaBancariaFX.contas.get(i).getTitular().getNome())) 
            {
               AgenciaBancariaFX.contas.get(i).setSaldo(AgenciaBancariaFX.contas.get(i).getSaldo() + Integer.parseInt(valor.getText()));
               saldo.setText("" + AgenciaBancariaFX.contas.get(i).getSaldo());
            }
        }
    }
        
        
    public void debitar(){
        for(int i=0;i<AgenciaBancariaFX.quantContas;i++)
        {
            if(AgenciaBancariaFX.pessoaEntrarConta.getNome().equals(AgenciaBancariaFX.contas.get(i).getTitular().getNome())) 
            {
               if(AgenciaBancariaFX.contas.get(i).getSaldo() >= Integer.parseInt(valor.getText()))
                {
                    AgenciaBancariaFX.contas.get(i).setSaldo(AgenciaBancariaFX.contas.get(i).getSaldo() - Integer.parseInt(valor.getText()));
                    saldo.setText("" + AgenciaBancariaFX.contas.get(i).getSaldo());
                }
               else{
                   System.out.println("VOCÊ NÃO TEM DINHEIRO SUFICIENTE PARA DEBITAR ESSE VALOR");
                   AgenciaBancariaFX.trocaTela("TelaInicialAgenciaBancaria.fxml");
               }
            }
        }
        
    }
            
    
}
